// Require the packages we will use:
var http = require('http'),
	socketio = require('socket.io'),
	url = require('url'),
	path = require('path'),
	mime = require('mime'),
	fs = require('fs'),
	express = require("express");

var mostRecent = "";
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	file = url.parse(req.url).pathname;
	if (file=="/") {
        file="/client.html";
    }
	var filename = path.join(__dirname, "", file);
	mostRecent = req.url.split("=")[1] == undefined ? mostRecent : req.url.split("=")[1];
        console.log("hi"+ mostRecent);
	(fs.exists || path.exists)(filename, function(exists){
		if (exists) {
			fs.readFile(filename, function(err, data){
				if (err) {
					// File exists but is not readable (permissions issue?)
					resp.writeHead(500, {
						"Content-Type": "text/plain"
					});
					resp.write("Internal server error: could not read file");
					resp.end();
					return;
				}
 
				// File exists and is readable
				var mimetype = mime.lookup(filename);
				resp.writeHead(200, {
					"Content-Type": mimetype
				});
				resp.write(data);
				resp.end();
				return;
			});
		}else{
			// File does not exist
			resp.writeHead(404, {
				"Content-Type": "text/plain"
			});
			resp.write("Requested file not found: "+filename);
			resp.end();
			return;
		}
	});
});
app.listen(3454);
// Do the Socket.IO magic:
var io = socketio.listen(app);

var people = {};
var clients = {};
io.sockets.on("connection", function(client){
    console.log("hi2"+ mostRecent);
	client.on('adduser', function(username){
	console.log("hi3"+ mostRecent);	
	    client.username = mostRecent;
		client.room = "";
		people[username] = client.room;
		clients[username] = client;
		client.join("start");
		
	});

	client.on("join", function(data){
		var name = data['name']
		people[client.username]=name;
		client.broadcast.to(client.room).emit('message_to_client', {name:'lobby', message: ""+ client.username+ " has left this lobby"})
		client.broadcast.to(client.room).emit('Users',{users:people,room:client.room});
		client.room = name;
		client.join(client.room);
		client.emit('message_to_client', {name:'lobby', message:'you have connected the Lobby: '+name});
		client.broadcast.to(client.room).emit('message_to_client', {name:'lobby', message: ""+ client.username+ " has connected the this Lobby"});
		client.broadcast.to(client.room).emit('Users',{users:people,room:client.room,owner:""});
		client.emit('Users',{users:people,room:client.room,owner:""});
		console.log("User Connected to "+name);
	});
	client.on("message_to_server", function(data){
		// This callback runs when the server receives a new message from the client.
		console.log("message: "+data["message"] +" to " + client.room); // log it to the Node.JS output
		
		io.sockets.in(client.room).emit("message_to_client",{name:client.username, message:data["message"] }) // broadcast the message to other users
	});
	client.on("whisper", function(data){
		var user = data["usr"];
		// This callback runs when the server receives a new message from the client.
		console.log("private message: "+data["message"] +" to " + user);
		var mess = data['message'].split(" ");
		console.log(mess);
		mess.splice(0,2);
		mess = mess.join(" ");
		console.log(mess);
		if (clients[user]==null) {
            client.emit("whisper",{user:"SERVER",message:"Username "+ user+ " not found"});
        }
		else{
			clients[user].emit("whisper",{name:client.username,message:mess});
			client.emit("whisper",{name:"to " + user, message:mess});
		}
	});
	client.on("removeUser", function(){
		delete people[client.id];
		io.sockets.emit("update-people", people);
	});
	client.on("disconnect", function(){
		io.sockets.emit("removeUser");
		io.sockets.emit("userUpdate", client.name, " disconnected from room");
	});
});
