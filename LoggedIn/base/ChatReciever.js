 $(document).ready(function() {
			var socketio = io.connect();
			$("#input").hide();
			$(".user").hide();
			$("#userListDiv").hide();
			$("#chat")
			$("h3").hide();
			$("#hrTitle").hide();
			$("#userList").hide();
			var client;
			start();
			function start() {
               var $username = "";
			   client = $username;
			   $("#login").fadeOut(500);
			   $("#input").fadeIn(500);
			   $("#startChatting").fadeIn(500);
			   $("#welcomeUser").text($username);
			   $("#userListDiv").fadeIn(500);
			   $("#userListTitle").fadeIn(500);
			   $("#userList").fadeIn(500);
			   $(".user").fadeIn(500);
			   $("#chatTitle").fadeIn(500);
			   $("#hrTitle").fadeIn(500);
			   socketio.emit('adduser', $username);
			   socketio.emit('join',{name:"General",pass:""});
            }

			
			
			socketio.on("message_to_client",function(data) {
			   //Append an HR thematic break and the escaped HTML of the new message
			   document.getElementById("chatConvo").appendChild(document.createTextNode(data['name']+ ": " + data['message']+" "));
			   $("#chatConvo").append("<a class='messageLike'> like </a>");
			   document.getElementById("chatConvo").appendChild(document.createElement("hr"));
			   updateScroll("chatConvo");
			});
			socketio.on("whisper",function(data) {
			   $("#chatConvo").append("<span class='whisper'>"+data['name']+ ": " + data['message']+" </span>");
			   $("#chatConvo").append("<a class='messageLike'> like </a>");
			   document.getElementById("chatConvo").appendChild(document.createElement("hr"));
			   updateScroll("chatConvo");
			});
			socketio.on("userUpdate",function(user, status) {
			   document.getElementById("chatConvo").appendChild(document.createTextNode(user + status));
			   document.getElementById("chatConvo").appendChild(document.createElement("hr"));
			   updateScroll("chatConvo");
			});
			
			socketio.on("Users", function(data){ //used http://www.tamas.io/simple-chat-application-using-node-js-and-socket-io/
				  people = data['users'];
				  room = data['room'];
				  $("#userList").empty();
				  console.log(room + " ;" +people);
				  $.each(people, function(clientid, name){
					 if (name==room) {
						var newNode = document.createElement('li');
						newNode.className = "userList";
						newNode.innerHTML = clientid
						document.getElementById("userList").appendChild(newNode);
						document.getElementById("userList").appendChild(document.createElement("br"));
                     }
				  });
			});
	  
			socketio.on("clearLog", function(){
			    $("#chatConvo").empty();
			});
			
			function sendMessage(){
			   var msg = document.getElementById("message_input").value;
			   var reg = new RegExp("^\\\w |\\\whisper");
			   if (reg.test(msg)) {
				  var user = msg.split(" ");
				  user = user[1];
				  socketio.emit("whisper",{message:msg,usr:user})
               }
			   else{
			   socketio.emit("message_to_server", {message:msg});
			   }
			   document.getElementById("message_input").value = "";
			}
			//updates the scroll to the bottom only if user hasn't scrolled up
			function updateScroll(el){
			   var element = document.getElementById(el);
			   var currHeight = element.scrollHeight - element.clientHeight;
			   //allowing for margin of error.
			   var atBottom =  currHeight <= element.scrollTop+50;
			   if (atBottom) {
				  var pane = document.getElementById(el);
				  pane.scrollTop = pane.scrollHeight;
			   }
			}
			
			
			
			$('#send').click(function(event){
			   sendMessage();
			   return false
			});
			
			
			$('#chatConvo').on('click','a',function(){
			   var reg = new RegExp("\\d+");
			   likes = this.innerHTML;
			   count = reg.exec(likes);
			   if (count!=null) {
                count = parseInt(count)+1
               }
			   else{
				  count = 1;
			   }
			   console.log(count);
			   this.innerHTML = "Likes ("+(count)+")";
			   
			})
			
			$('#message_input').keydown(function(event) {
			  if (event.keyCode == 13) {
				  sendMessage();
				  return false;
			   }
			});
    
		 });
