(function () {
    'use strict';

    angular
        .module('app')
        .controller('MainController', MainController);
    MainController.$inject = ['UserService', '$rootScope','$sce'];
    function MainController(UserService, $rootScope,$sce) {
        var vm = this;
        vm.trustSrc = function(src) {
	    return $sce.trustAsResourceUrl(src);
        }
        vm.user = null;
        vm.allUsers = [];   
        initController();
        vm.link = "http://54.218.0.90:3454/?="+$rootScope.globals.currentUser.username;
        function initController() {
            loadCurrentUser();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                    vm.scores = [];
                });
        }
    }
    
    
   
    

})();
