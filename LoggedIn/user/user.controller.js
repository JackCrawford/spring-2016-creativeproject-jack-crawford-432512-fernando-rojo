(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserController', UserController);

    UserController.$inject = ['UserService', '$rootScope','$sce'];
    function UserController(UserService, $rootScope,$sce) {
        var vm = this;
	vm.trustSrc = function(src) {
	    return $sce.trustAsResourceUrl(src);
	}
        vm.user = null;
        vm.allUsers = [];
        vm.deleteUser = deleteUser;

        initController();
        vm.link = "http://54.218.0.90:3454/?="+$rootScope.globals.currentUser.username;
	
        function initController() {
            loadCurrentUser();
            loadAllUsers();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }
        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }
    }

})();
